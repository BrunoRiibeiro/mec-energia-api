invalid_verifier_environment = pytest.mark.parametrize("environment", [
	("test"),
	("development"),
])

@invalid_verifier_environment
def test_rejects_environment(environment: str):
	with pytest.raises(Exception) as e:
		sut(environment)
	assert "Invalid ENVIRONMENT" in str(e.value)